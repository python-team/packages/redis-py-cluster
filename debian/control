Source: redis-py-cluster
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Nicolas Dandrimont <olasd@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-hiredis,
               python3-redis,
               python3-setuptools,
               python3-sphinx
Standards-Version: 3.9.8
Vcs-Git: https://salsa.debian.org/python-team/packages/redis-py-cluster.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/redis-py-cluster
Homepage: https://redis-py-cluster.readthedocs.io/

Package: python3-rediscluster
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests}
Description: Python interface to a cluster of Redis key-value stores
 Redis is a key-value database in a similar vein to memcache but the dataset
 is non-volatile. Redis additionally provides native support for atomically
 manipulating and querying data structures such as lists and sets.
 .
 redis-py-cluster provides Python bindings to Redis Cluster, the distributed
 implementation of the Redis key-value store, available upstream since Redis
 3.0.
 .
 This package provides the Python 3 version of the rediscluster module.

Package: python-rediscluster-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: Python interface to a cluster of Redis key-value stores - Documentation
 Redis is a key-value database in a similar vein to memcache but the dataset
 is non-volatile. Redis additionally provides native support for atomically
 manipulating and querying data structures such as lists and sets.
 .
 redis-py-cluster provides Python bindings to Redis Cluster, the distributed
 implementation of the Redis key-value store, available upstream since Redis
 3.0.
 .
 This package provides the documentation for the rediscluster module
